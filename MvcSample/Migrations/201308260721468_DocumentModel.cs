namespace MvcSample.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DocumentModel : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Documents",
                c => new
                    {
                        DocumentID = c.Guid(nullable: false),
                        Title = c.String(),
                        Descript = c.String(),
                        Tags = c.String(),
                        Createtime = c.DateTime(nullable: false),
                        Updatetime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.DocumentID);
            
            CreateTable(
                "dbo.DocumentContents",
                c => new
                    {
                        DocumentContentID = c.Guid(nullable: false),
                        DocumentID = c.Guid(nullable: false),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.DocumentContentID)
                .ForeignKey("dbo.Documents", t => t.DocumentID, cascadeDelete: true)
                .Index(t => t.DocumentID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.DocumentContents", new[] { "DocumentID" });
            DropForeignKey("dbo.DocumentContents", "DocumentID", "dbo.Documents");
            DropTable("dbo.DocumentContents");
            DropTable("dbo.Documents");
        }
    }
}
