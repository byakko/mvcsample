namespace MvcSample.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AgtUserMains",
                c => new
                    {
                        AgtUserMainID = c.Guid(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 50),
                        Password = c.String(),
                        Agentname = c.String(),
                        Fax = c.String(),
                        SysProvinceID = c.Int(),
                        SysCityID = c.Int(),
                        Status = c.Int(nullable: false),
                        IsDel = c.Boolean(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Createtime = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.AgtUserMainID)
                .ForeignKey("dbo.SysCities", t => t.SysCityID)
                .ForeignKey("dbo.SysProvinces", t => t.SysProvinceID)
                .Index(t => t.SysCityID)
                .Index(t => t.SysProvinceID);
            
            CreateTable(
                "dbo.SysProvinces",
                c => new
                    {
                        SysProvinceID = c.Int(nullable: false, identity: true),
                        ZIP = c.Int(nullable: false),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.SysProvinceID);
            
            CreateTable(
                "dbo.SysCities",
                c => new
                    {
                        SysCityID = c.Int(nullable: false, identity: true),
                        ZIP = c.Int(nullable: false),
                        Name = c.String(),
                        SysProvinceID = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.SysCityID)
                .ForeignKey("dbo.SysProvinces", t => t.SysProvinceID, cascadeDelete: true)
                .Index(t => t.SysProvinceID);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.SysCities", new[] { "SysProvinceID" });
            DropIndex("dbo.AgtUserMains", new[] { "SysProvinceID" });
            DropIndex("dbo.AgtUserMains", new[] { "SysCityID" });
            DropForeignKey("dbo.SysCities", "SysProvinceID", "dbo.SysProvinces");
            DropForeignKey("dbo.AgtUserMains", "SysProvinceID", "dbo.SysProvinces");
            DropForeignKey("dbo.AgtUserMains", "SysCityID", "dbo.SysCities");
            DropTable("dbo.SysCities");
            DropTable("dbo.SysProvinces");
            DropTable("dbo.AgtUserMains");
        }
    }
}
