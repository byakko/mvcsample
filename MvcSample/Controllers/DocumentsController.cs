﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MvcSample.Models;
using MvcSample.DAL;

namespace MvcSample.Controllers
{
    public class DocumentsController : ApiController
    {
        private SampleContext db = new SampleContext();

        // GET api/Documents
        public IEnumerable<Document> GetDocuments()
        {
            return db.Documents.AsEnumerable();
        }

        // GET api/Documents/5
        public Document GetDocument(Guid id)
        {
            Document document = db.Documents.Find(id);
            if (document == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return document;
        }

        // PUT api/Documents/5
        public HttpResponseMessage PutDocument(Guid id, Document document)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != document.DocumentID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(document).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/Documents
        public HttpResponseMessage PostDocument(Document document)
        {
            if (ModelState.IsValid)
            {
                db.Documents.Add(document);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, document);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = document.DocumentID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/Documents/5
        public HttpResponseMessage DeleteDocument(Guid id)
        {
            Document document = db.Documents.Find(id);
            if (document == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.Documents.Remove(document);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, document);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}