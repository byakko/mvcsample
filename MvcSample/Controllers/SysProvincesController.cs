﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MvcSample.Models;
using MvcSample.DAL;

namespace MvcSample.Controllers
{
    public class SysProvincesController : ApiController
    {
        private SampleContext db = new SampleContext();

        // GET api/SysProvinces
        public IEnumerable<SysProvince> GetSysProvinces()
        {
            return db.SysProvinces.AsEnumerable();
        }

        // GET api/SysProvinces/5
        public SysProvince GetSysProvince(int id)
        {
            SysProvince sysprovince = db.SysProvinces.Find(id);
            if (sysprovince == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return sysprovince;
        }

        // PUT api/SysProvinces/5
        public HttpResponseMessage PutSysProvince(int id, SysProvince sysprovince)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            if (id != sysprovince.SysProvinceID)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest);
            }

            db.Entry(sysprovince).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/SysProvinces
        public HttpResponseMessage PostSysProvince(SysProvince sysprovince)
        {
            if (ModelState.IsValid)
            {
                db.SysProvinces.Add(sysprovince);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, sysprovince);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = sysprovince.SysProvinceID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/SysProvinces/5
        public HttpResponseMessage DeleteSysProvince(int id)
        {
            SysProvince sysprovince = db.SysProvinces.Find(id);
            if (sysprovince == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            db.SysProvinces.Remove(sysprovince);

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, sysprovince);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}