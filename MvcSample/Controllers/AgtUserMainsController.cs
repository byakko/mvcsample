﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using MvcSample.Models;
using MvcSample.DAL;

namespace MvcSample.Controllers
{
    public class AgtUserMainsController : ApiController
    {
        private SampleContext db = new SampleContext();

        // GET api/AgtUserMains
        public object GetAgtUserMains()
        {
            var agtusermains = db.AgtUserMains.Where(u => u.IsDel == false);
            int page = Int32.Parse(HttpContext.Current.Request.QueryString.GetValues("page")[0]);
            page = page < 1 ? 1 : page;
            int skipNum = 10 * (page - 1);
            return new
            {
                totalLength = agtusermains.Count(),
                agt_users = agtusermains.OrderBy(u => u.Createtime).Skip(skipNum).Take(10).AsEnumerable()
            };
        }

        // GET api/AgtUserMains/5
        public AgtUserMain GetAgtUserMain(Guid id)
        {
            AgtUserMain agtusermain = db.AgtUserMains.Find(id);
            if (agtusermain == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            return agtusermain;
        }

        public object GetValid(bool Valid, string property, string value, Guid? id)
        {
            if (property == "UserName")
            {
                var condition = db.AgtUserMains.Where(u => u.UserName == value);
                if (id != null)
                {
                    condition = condition.Where(u => u.AgtUserMainID != id);
                }
                AgtUserMain agtusermain = condition.FirstOrDefault();
                if (agtusermain != null)
                {
                    return new
                    {
                        isVaild = false,
                        message = "用户名已存在"
                    };
                }
            }
            return new
            {
                isVaild = true
            };
        }

        // PUT api/AgtUserMains/5
        public HttpResponseMessage PutAgtUserMain(Guid id, AgtUserMain agtusermain)
        {
            if (!ModelState.IsValid)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }

            AgtUserMain agtusermainEntity = db.AgtUserMains.Find(id);
            if (agtusermainEntity == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            agtusermainEntity.UserName = agtusermain.UserName;
            agtusermainEntity.Agentname = agtusermain.Agentname;
            agtusermainEntity.SysCityID = agtusermain.SysCityID;
            agtusermainEntity.SysProvinceID = agtusermain.SysProvinceID;
            agtusermainEntity.IsActive = agtusermain.IsActive;
            agtusermainEntity.Fax = agtusermain.Fax;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // PUT api/AgtUserMains/5?resetPwd=true
        public HttpResponseMessage PutResetPassword(Guid id, bool resetPwd)
        {
            AgtUserMain agtusermain = db.AgtUserMains.Find(id);
            if (agtusermain == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }

            agtusermain.Password = "777777";

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        // POST api/AgtUserMains
        public HttpResponseMessage PostAgtUserMain(AgtUserMain agtusermain)
        {
            if (ModelState.IsValid)
            {
                db.AgtUserMains.Add(agtusermain);
                db.SaveChanges();

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, agtusermain);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = agtusermain.AgtUserMainID }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
        }

        // DELETE api/AgtUserMains/5
        public HttpResponseMessage DeleteAgtUserMain(Guid id)
        {
            AgtUserMain agtusermain = db.AgtUserMains.Find(id);
            if (agtusermain == null)
            {
                return Request.CreateResponse(HttpStatusCode.NotFound);
            }

            agtusermain.IsDel = true;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK, agtusermain);
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}