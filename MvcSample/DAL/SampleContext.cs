﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcSample.Models;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace MvcSample.DAL
{
    public class SampleContext : DbContext
	{
		public DbSet<AgtUserMain> AgtUserMains { get; set; }
		public DbSet<SysProvince> SysProvinces { get; set; }
		public DbSet<SysCity> SysCities { get; set; }
        public DbSet<Document> Documents { get; set; }
        public DbSet<DocumentContent> DocumentContents { get; set; } 
	}
}