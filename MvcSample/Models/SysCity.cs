﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcSample.Models
{
	public class SysCity
	{
		public int SysCityID { get; set; }
		public int ZIP { get; set; }
		public string Name { get; set; }
		public int SysProvinceID { get; set; }

		//public virtual SysProvince SysProvince { get; set; }
	}
}