using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MvcSample.Models
{
	public class DocumentContent
	{
		public Guid DocumentContentID { get; set; }
		public Guid DocumentID { get; set; }
		public string Content { get; set; }

		public virtual Document Document { get; set; }
	}
}