using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MvcSample.Models
{
	public class Document
	{
		public Guid DocumentID { get; set; }
		public string Title { get; set; }
		public string Descript { get; set; }
        public string Tags { get; set; }
		public DateTime Createtime { get; set; }
		public DateTime Updatetime { get; set; }

        public virtual ICollection<DocumentContent> DocumentContents { get; set; }

        public Document()
        {
            DocumentID = Guid.NewGuid();
            Createtime = DateTime.Now;
            Updatetime = DateTime.Now;
        }
	}
}