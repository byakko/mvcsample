﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MvcSample.Models
{
	public class SysProvince
	{
		public int SysProvinceID { get; set; }
		public int ZIP { get; set; }
		public string Name { get; set; }

		public virtual ICollection<SysCity> SysCities { get; set; }
	}
}