﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MvcSample.Models
{
    public class AgtUserMain
    {
        public Guid AgtUserMainID { get; set; }

        [Required, StringLength(50)]
        [CustomValidation(typeof(AgtUserMain), "IsUserNameUnique")]
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Agentname { get; set; }
        public string Fax { get; set; }
        public int? SysProvinceID { get; set; }
        public int? SysCityID { get; set; }
        public int Status { get; set; }
        public bool IsDel { get; set; }
        public bool IsActive { get; set; }
        public DateTime Createtime { get; set; }

        public virtual SysProvince SysProvince { get; set; }
        public virtual SysCity SysCity { get; set; }

        public AgtUserMain()
        {
            AgtUserMainID = Guid.NewGuid();
            Password = "666666";
            Createtime = DateTime.Now;
        }

        public static ValidationResult IsUserNameUnique(string value, ValidationContext validationContext)
        {
            if (value == "walter")
            {
                return new ValidationResult("用户名已存在。");
            }
            return ValidationResult.Success;
        }
    }
}