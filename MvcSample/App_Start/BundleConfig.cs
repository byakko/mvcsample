﻿using System.Web;
using System.Web.Optimization;

namespace MvcSample
{
    public class BundleConfig
    {
        // 有关 Bundling 的详细信息，请访问 http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/indexeddb").Include(
                        "~/Scripts/lib/jquery.indexeddb.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/flatui-plugin").Include(
                        "~/Scripts/lib/flatui-checkbox.js",
                        "~/Scripts/lib/flatui-radio.js"));

            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
                        "~/Scripts/knockout-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/ajaxlogin").Include(
                "~/Scripts/app/ajaxlogin.js"));

            bundles.Add(new ScriptBundle("~/bundles/lib").Include(
                "~/Scripts/jquery-2.0.3.js",
                "~/Scripts/jquery-ui-1.10.3.js",
                "~/Scripts/lib/jquery.indexeddb.js",
                "~/Scripts/knockout-2.3.0.js",
                "~/Scripts/knockout.validation.js",
                "~/Scripts/underscore.js",
                "~/Scripts/bootstrap.js",
                "~/Scripts/lib/bootstrap-select.js",
                "~/Scripts/lib/bootstrap-switch.js",
                "~/Scripts/lib/flatui-checkbox.js",
                "~/Scripts/lib/flatui-radio.js",
                "~/Scripts/lib/messenger.js"));

            bundles.Add(new ScriptBundle("~/bundles/shared").Include(
                "~/Scripts/app/shared/setting.js",
                "~/Scripts/app/shared/adapter.js",
                "~/Scripts/app/shared/confirmtask.js",
                "~/Scripts/app/shared/pagination.js",
                "~/Scripts/app/shared/modelhelp.js",
                "~/Scripts/app/shared/collectionhelp.js",
                "~/Scripts/app/shared/common.js"));

            bundles.Add(new ScriptBundle("~/bundles/showgrid").Include(
                "~/Scripts/app/showgrid/common.js",
                "~/Scripts/app/showgrid/models/user.js",
                "~/Scripts/app/showgrid/models/usercollection.js",
                "~/Scripts/app/showgrid/viewmodels/showgrid.js",
                "~/Scripts/app/showgrid/main.js"));

            bundles.Add(new ScriptBundle("~/bundles/todo").Include(
                "~/Scripts/app/todo.bindings.js",
                "~/Scripts/app/todo.datacontext.js",
                "~/Scripts/app/todo.model.js",
                "~/Scripts/app/todo.viewmodel.js"));

            // 使用要用于开发和学习的 Modernizr 的开发版本。然后，当你做好
            // 生产准备时，请使用 http://modernizr.com 上的生成工具来仅选择所需的测试。
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/Site.css",
                "~/Content/TodoList.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new StyleBundle("~/Content/themes/all").Include(
                "~/Content/bootstrap.css",
                "~/Content/bootstrap-responsive.css",
                "~/Content/themes/flat-ui/css/flat-ui.css",
                "~/Content/themes/messenger/messenger.css",
                "~/Content/themes/messenger/messenger-theme-air.css"));

            bundles.Add(new StyleBundle("~/Content/themes/bootstrap").Include(
                "~/Content/bootstrap.css",
                "~/Content/bootstrap-responsive.css"));

            bundles.Add(new StyleBundle("~/Content/themes/flat-ui").Include(
                "~/Content/themes/flat-ui/css/flat-ui.css"));

            bundles.Add(new StyleBundle("~/Content/themes/messenger").Include(
                "~/Content/themes/messenger/messenger.css",
                "~/Content/themes/messenger/messenger-theme-air.css"));
        }
    }
}