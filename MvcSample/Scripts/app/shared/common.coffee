app = app or {}
app.Shared = app.Shared or {}

app.Shared.Common = class
	@alert = (content, type) =>
		Messenger().post
			message: content
			type: type
			hideAfter: 1.5
			showCloseButton: true

	@alertError = =>
		@alert '服务器异常', 'error'