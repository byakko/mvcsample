app.ViewModel = app.ViewModel or {}

app.ViewModel.ShowGrid = class
	constructor: ->
		@userCollection = new app.Model.UserCollection

		@flush = app.Common.confirmTask.flush

		@cancel = app.Common.confirmTask.cancel

		@userCollection.help.load
			page: 1