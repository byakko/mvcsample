Messenger.options = {
	extraClasses: 'messenger-fixed messenger-on-bottom messenger-on-right',
	theme: 'air'
}

app.Common = new class
	constructor: ->
		@confirmTask = new app.Shared.ConfirmTask

		@alert = (content, type) =>
			Messenger().post
				message: content
				type: type
				hideAfter: 1.5
				showCloseButton: true

		@alertError = =>
			@alert '服务器异常', 'error'
