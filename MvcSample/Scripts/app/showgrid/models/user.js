// Generated by CoffeeScript 1.6.3
app.Model = app.Model || {};

app.Model.User = (function() {
  function _Class(data) {
    var _this = this;
    this.help = new app.Shared.ModelHelp(this);
    this.help.init(data);
    this.formatTime = ko.computed(function() {
      return new Date(_this.Createtime()).toLocaleString();
    });
    this.formatIsActive = ko.computed(function() {
      if (_this.IsActive()) {
        return '是';
      } else {
        return '否';
      }
    });
    this.errors = ko.validation.group(this);
  }

  _Class.prototype.key = 'AgtUserMainID';

  _Class.prototype.url = '/api/AgtUserMains';

  _Class.prototype.persist = ['UserName', 'Agentname', 'Fax', 'SysProvinceID', 'SysCityID', 'IsActive', 'Createtime'];

  _Class.prototype.activeVaild = function() {
    this.UserName.extend({
      required: {
        message: '请输入用户名'
      },
      maxLength: {
        params: 20,
        message: '用户名过长'
      }
    });
    return this.Agentname.extend({
      maxLength: {
        params: 20,
        message: '代理商名称过长'
      }
    });
  };

  _Class.prototype["delete"] = function(callback) {
    return this.help["delete"]().done(callback);
  };

  _Class.prototype.resetPwd = function(callback) {
    return $.ajax(this.help.requestUrl() + '?resetPwd=true', {
      type: 'PUT'
    }).done(callback);
  };

  return _Class;

})();
