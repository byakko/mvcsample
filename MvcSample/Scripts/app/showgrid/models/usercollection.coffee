app.Model = app.Model or {}

app.Model.UserCollection = class
	constructor: ->
		@help = new app.Shared.CollectionHelp @
		@help.init()

		@$formBench = $('#formBench')

		@userBench = ko.observable()
		@benchType = ko.observable()

		@provs = ko.observableArray []
		@cities = ko.observableArray []

		@provLoad()

		@pagination = new app.Shared.Pagination @

	model: app.Model.User
	dataRoot: 'agt_users'

	add: ->
		user = @help.new {}
		@provChange(user)
		@userBench user
		user.activeVaild()
		do @$formBench.find('[data-toggle="checkbox"]').checkbox
		@benchType 'create'

	update: (user) ->
		user.help.fetch().done((data) =>
			user = @help.new data
			@provChange(user)
			@userBench user
			user.activeVaild()
			do @$formBench.find('[data-toggle="checkbox"]').checkbox
			@benchType 'update'
		).fail (data) =>
			do app.Common.alertError

	submit: (user) ->
		switch @benchType()
			when 'create'
				successMessage = '用户已添加'
			when 'update'
				successMessage = '修改成功'
		user.help.saveChanges().done((data, status) =>
			app.Common.alert successMessage, 'success'
			@pagination.reload()
			@$formBench.modal('hide')
		).fail (data, status) =>
			app.Common.alert(data.responseJSON.Message, 'error')
			@help.messageRender data, user

	resetPwd: (user) ->
		app.Common.confirmTask.push user.resetPwd, user, (data, status) =>
			app.Common.alert '密码已重置', 'success' if status == 'success'

	delete: (user) ->
		app.Common.confirmTask.push user.delete, user, (data, status) =>
			app.Common.alert '删除成功', 'success' if status == 'success'
			@pagination.reload()

	provLoad: ->
		$.getJSON "/api/SysProvinces", (provs) =>
			@provs provs

	provChange: (user) ->
		selectedProv = ko.utils.arrayFilter(@provs(), (prov) ->
			prov.ZIP == user.SysProvinceID()
		)[0]
		cities = if selectedProv then selectedProv.SysCities else []
		@cities cities